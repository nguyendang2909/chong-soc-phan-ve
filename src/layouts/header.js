import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';

const { Header } = Layout;

function PageHeader() {
  return (
    <Header className="header" style={{ background: '#fff', padding: 0, paddingLeft: 16 }}>
      <div className="logo" />
      <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
        <Menu.Item key="1">
          Trang chủ
          <Link to="/" />
        </Menu.Item>
        <Menu.Item key="2">
          <span>Giới thiệu</span>
          <Link to="/gioi-thieu" />
        </Menu.Item>
        <Menu.Item key="3">
          <span>Danh mục chữ viết tắt</span>
          <Link to="/danh-muc-chu-viet-tat" />
        </Menu.Item>
      </Menu>
    </Header>
  );
}

export default PageHeader;
